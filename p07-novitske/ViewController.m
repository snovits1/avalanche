//
//  ViewController.m
//  p07-novitske
//
//  Created by Steven Novitske on 4/20/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//
// icicle image address: https://img.clipartfest.com/d324c1565291dbb40f6f1683908f6f0c_download-this-image-as-animated-icicle-clipart_270-595.png
//
// penguin image address: http://www.how-to-draw-cartoons-online.com/image-files/xcartoon_penguins.gif.pagespeed.ic.bZVzf9Ag-B.png
//
// coin image address: https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjphMSAi8_TAhUh4oMKHaKACSAQjRwIBw&url=http%3A%2F%2Fpngimg.com%2Fdownload%2F11041&psig=AFQjCNH7xrf2SL_ZHxnVT0Z8-ZwFvFi1Jw&ust=1493741530179882
//
// blood image address:

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController ()

@end

int screenHeight;
int screenWidth;
int score;
CADisplayLink* display;
UIButton* left;
UIButton* right;
bool goLeft;
bool goRight;
UIImageView* runner;
NSMutableArray* icicles;
bool falling[20];
int speeds[20];
UIButton* newGame;
int level;
UIStepper* levelPicker;
UILabel* levelLabel;
UILabel* scoreLabel;
UIImageView* coin;
UIImageView* blood;
AVAudioPlayer *ching;
AVAudioPlayer *hit;


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    screenHeight = self.view.frame.size.height;
    screenWidth = self.view.frame.size.width;
    score = 0;
    
    //Background color gradient
    CAGradientLayer* gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:18/255.0 green:155/255.0 blue:234/255.0 alpha:1].CGColor,
                        (id)[UIColor colorWithWhite:1 alpha:1].CGColor];
    
    //Set up display link
    display = [CADisplayLink displayLinkWithTarget:self selector:@selector(mainRoutine)];
    display.preferredFramesPerSecond = 60;
    [display addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    
    //Set up move left button
    left = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, screenWidth/2, screenHeight)];
    [left addTarget:self action:@selector(moveLeft) forControlEvents:UIControlEventTouchDown];
    [left addTarget:self action:@selector(released) forControlEvents:UIControlEventTouchUpInside];
    
    //Set up move right button
    right = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth/2, 0, screenWidth/2, screenHeight)];
    [right addTarget:self action:@selector(moveRight) forControlEvents:UIControlEventTouchDown];
    [right addTarget:self action:@selector(released) forControlEvents:UIControlEventTouchUpInside];
    
    goLeft = NO;
    goRight = NO;
    
    //Set up runner
    runner = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth*0.05, screenWidth*0.1)];
    runner.center = CGPointMake(screenWidth/2, screenHeight*0.90);
    [runner setImage:[UIImage imageNamed:@"rightPenguin.png"]];
    //Some image appeareance optimization
    [runner.layer setMinificationFilter:kCAFilterTrilinear];
    runner.layer.shouldRasterize = YES;
    runner.layer.rasterizationScale = 1.2;
    
    //Set up new game button
    newGame = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth*0.3, screenHeight*0.4, screenWidth*0.4, screenHeight*0.2)];
    newGame.layer.cornerRadius = 10;
    newGame.layer.masksToBounds = YES;
    newGame.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    newGame.titleLabel.font = [UIFont boldSystemFontOfSize:screenHeight*0.1];
    [newGame setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [newGame setTitle:@"Start" forState:UIControlStateNormal];
    [newGame addTarget:self action:@selector(newGamePress:) forControlEvents:UIControlEventTouchDown];
    [newGame addTarget:self action:@selector(restartGame:) forControlEvents:UIControlEventTouchUpInside];
    
    //Set up level picker
    levelPicker = [[UIStepper alloc] init];
    levelPicker.center = CGPointMake(screenWidth/2, screenHeight*0.3);
    levelPicker.minimumValue = 1;
    levelPicker.maximumValue = 100;
    levelPicker.value = 1;
    levelPicker.wraps = YES;
    levelPicker.tintColor = [UIColor blackColor];
    levelPicker.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    levelPicker.layer.cornerRadius = 5;
    levelPicker.layer.masksToBounds = YES;
    levelLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth/2-47, levelPicker.frame.origin.y-25, 94, 20)];
    levelLabel.layer.cornerRadius = 5;
    levelLabel.layer.masksToBounds = YES;
    levelLabel.text = [NSString stringWithFormat:@"Level: %.0lf",levelPicker.value];
    levelLabel.textAlignment = NSTextAlignmentCenter;
    levelLabel.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    level = levelPicker.value;
    
    //Set up coin behind icicles
    coin = [[UIImageView alloc] initWithFrame:CGRectMake(rand()%(int)(screenWidth*0.97), 0, screenWidth*0.03, screenWidth*0.03)];
    coin.image = [UIImage imageNamed:@"coin.png"];
    coin.center = CGPointMake(coin.center.x, screenHeight*0.93);
    [coin setHidden:YES];
    [self.view addSubview:coin];
    //Some image appeareance optimization
    [coin.layer setMinificationFilter:kCAFilterTrilinear];
    coin.layer.shouldRasterize = YES;
    coin.layer.rasterizationScale = 1.2;
    
    //Set up blood splatter
    blood = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth*0.07, screenWidth*0.07)];
    blood.image = [UIImage imageNamed:@"blood.png"];
    [blood setHidden:YES];
    
    //Set up icicles
    icicles = [[NSMutableArray alloc] init];
    for(int i=0; i<20; i++) {
        UIImageView *icicle = [[UIImageView alloc] initWithFrame:CGRectMake(i*screenWidth*0.05, 0, screenWidth*0.05, screenWidth*0.1)];
        [icicle setImage:[UIImage imageNamed:@"icicle.png"]];
        
        //Some image appeareance optimization
        [icicle.layer setMinificationFilter:kCAFilterTrilinear];
        icicle.layer.shouldRasterize = YES;
        icicle.layer.rasterizationScale = 1.2;
        
        [icicles addObject:icicle];
        falling[i] = NO;
        speeds[i] = rand()%3 + level;
        [self.view addSubview:icicle];
    }
    
    //Set up score label
    scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth*0.3, newGame.frame.origin.y - 70, screenWidth*0.4, 50)];
    scoreLabel.layer.cornerRadius = 5;
    scoreLabel.layer.masksToBounds = YES;
    scoreLabel.text = [NSString stringWithFormat:@"Score: %d", score];
    scoreLabel.textAlignment = NSTextAlignmentCenter;
    scoreLabel.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
    scoreLabel.textColor = [UIColor colorWithRed:39/255.0 green:180/255.0 blue:47/255.0 alpha:1];
    scoreLabel.font = [UIFont boldSystemFontOfSize:scoreLabel.frame.size.height/2];
    [scoreLabel setHidden:YES];
    
    //Set up sounds
    NSString *path = [NSString stringWithFormat:@"%@/BELL.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    ching = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    path = [NSString stringWithFormat:@"%@/thwap.wav", [[NSBundle mainBundle] resourcePath]];
    soundUrl = [NSURL fileURLWithPath:path];
    hit = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
    //Add subviews
    [self.view.layer insertSublayer:gradient atIndex:0];
    [self.view addSubview:left];
    [self.view addSubview:right];
    [self.view addSubview:blood];
    [self.view addSubview:runner];
    [self.view addSubview:newGame];
    [self.view addSubview:levelPicker];
    [self.view addSubview:levelLabel];
    [self.view addSubview:scoreLabel];
}

-(void)mainRoutine {
    if(newGame.isHidden) {
        //Move runner left or right
        if(goLeft) runner.center = CGPointMake(runner.center.x-6, runner.center.y);
        else if(goRight) runner.center = CGPointMake(runner.center.x+6, runner.center.y);
        if(runner.center.x > screenWidth) runner.center = CGPointMake(0, runner.center.y);
        else if(runner.center.x < 0) runner.center = CGPointMake(screenWidth, runner.center.y);
        //Check if runner gets coin
        if(CGRectIntersectsRect(runner.frame, coin.frame)) {
            [ching play];
            score++;
            coin.frame = CGRectMake(rand()%(int)(screenWidth*0.97), 0, screenWidth*0.03, screenWidth*0.03);
            coin.center = CGPointMake(coin.center.x, screenHeight*0.93);
        }
        //Move icicles down at random times and speeds and detect collision with runner
        for(int i=0; i<20; i++) {
            int chance = rand();
            if(chance%150 == i) falling[i] = YES;
            UIView *icicle = icicles[i];
            if(falling[i]) icicle.center = CGPointMake(icicle.center.x, icicle.center.y + speeds[i]);
            if(CGRectContainsPoint(runner.frame, CGPointMake(icicle.center.x, icicle.center.y + icicle.frame.size.height/2))) {
                [hit play];
                [newGame setHidden:NO];
                scoreLabel.text = [NSString stringWithFormat:@"Score: %d", score];
                [scoreLabel setHidden:NO];
                blood.center = CGPointMake(icicle.center.x, icicle.center.y + icicle.frame.size.height/2);
                [blood setHidden:NO];
            }
            if(icicle.frame.origin.y > screenHeight) {
                icicle.frame = CGRectMake(i*screenWidth*0.05, 0, screenWidth*0.05, screenWidth*0.1);
                falling[i] = NO;
                speeds[i] = rand()%3 + level;
            }
        }
    } else {
        level = levelPicker.value;
        levelLabel.text = [NSString stringWithFormat:@"Level: %d",level];
        for(int i=0; i<20; i++) speeds[i] = rand()%3 + level;
    }
}

-(void)moveLeft {
    goLeft = YES;
    [runner setImage:[UIImage imageNamed:@"leftPenguin.png"]];
}
              
-(void)moveRight {
    goRight = YES;
    [runner setImage:[UIImage imageNamed:@"rightPenguin.png"]];
}

-(void)released {
    goLeft = NO;
    goRight = NO;
}

-(void)newGamePress:(UIButton*)sender {
    sender.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
}

-(void)restartGame:(UIButton*)sender {
    if([sender.titleLabel.text isEqual:@"Start"]) {
        [coin setHidden:NO];
        [levelPicker setHidden:YES];
        [levelLabel setHidden:YES];
        [sender setHidden:YES];
        [sender setTitle:@"New Game" forState:UIControlStateNormal];
        score = 0;
    } else if([sender.titleLabel.text isEqual:@"New Game"]) {
        for(int i=0; i<20; i++) {
            UIView *icicle = icicles[i];
            icicle.frame = CGRectMake(i*screenWidth*0.05, 0, screenWidth*0.05, screenWidth*0.1);
            falling[i] = NO;
            speeds[i] = rand()%3 + 3;
        }
        [blood setHidden:YES];
        [coin setHidden:YES];
        coin.frame = CGRectMake(rand()%(int)(screenWidth*0.97), 0, screenWidth*0.03, screenWidth*0.03);
        coin.center = CGPointMake(coin.center.x, screenHeight*0.93);
        [levelPicker setHidden:NO];
        [levelLabel setHidden:NO];
        [scoreLabel setHidden:YES];
        runner.center = CGPointMake(screenWidth/2, screenHeight*0.9);
        [sender setTitle:@"Start" forState:UIControlStateNormal];
    }
    sender.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
